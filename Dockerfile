FROM roundcube/roundcubemail:1.4.x-apache

LABEL maintainer Magnus Anderssen <magnus@magooweb.com>

ENV VERSION_DOVECOT_IDENT 2.2
ENV VERSION_CARDDAV v4.0.0

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
    mv /usr/src/roundcubemail/composer.json-dist /usr/src/roundcubemail/composer.json

RUN composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist --prefer-stable \
        --no-update --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        require \
            johndoh/contextmenu \
    && \
    composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist --prefer-stable \
        --no-update --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        require roundcube/carddav:${VERSION_CARDDAV} \
    && \
    composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist --prefer-stable \
        --no-update --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        require cor/dovecot-ident:${VERSION_DOVECOT_IDENT} \
    && \
    composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist --no-dev \
        --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        update;

RUN sed -i "s/REMOTE_ADDR/HTTP_X_FORWARDED_FOR/g" /usr/src/roundcubemail/plugins/dovecot_ident/dovecot_ident.php
