# davcube

This is just an extended version of the official roundcube image.

It adds:

- carddav
- dovecot-ident -- modified to use HTTP_X_FORWARDED_FOR instead of REMOTE_ADDR

## To use it

```sh
$ docker run -e ROUNDCUBEMAIL_DEFAULT_HOST=mail -e ROUNDCUBEMAIL_SMTP_SERVER=mail -d roundcube/roundcubemail
```

or define a `.env` file:

```
ROUNDCUBEMAIL_DEFAULT_HOST=ssl://mail.example.com
ROUNDCUBEMAIL_DEFAULT_PORT=993
ROUNDCUBEMAIL_SMTP_SERVER=tls://mail.example.com
ROUNDCUBEMAIL_SMTP_PORT=587
ROUNDCUBEMAIL_PLUGINS=carddav,contextmenu,zipdownload,managesieve
ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE=10M
```

and use it like so:

````
docker run -ti --env-file .env -p 8080:80 -v $(pwd)/.test/db:/var/roundcube/db:rw -v $(pwd)/.test/config:/var/roundcube/config test/roundcube
```


see [Roundcube docker image for more configuration information](https://hub.docker.com/r/roundcube/roundcubemail).

My usage example in docker-compose:

```yaml
web-mail:
  image: callmemagnus/roundavcube
  container_name: web-mail
  restart: always
  environment:
    - VIRTUAL_HOST=mail.example.com
    - VIRTUAL_PORT=80
    - LETSENCRYPT_HOST=mail.example.com
    - LETSENCRYPT_EMAIL=certmaster@example.com
    - ROUNDCUBEMAIL_DEFAULT_HOST=ssl://mailserver
    - ROUNDCUBEMAIL_DEFAULT_PORT=993
    - ROUNDCUBEMAIL_SMTP_SERVER=tls://mailserver
    - ROUNDCUBEMAIL_SMTP_PORT=587
    - ROUNDCUBEMAIL_PLUGINS=carddav,contextmenu,zipdownload,managesieve
    - ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE=10M
  volumes:
    - /srv/docker/roundcube/db:/var/roundcube/db:rw
    - /srv/docker/roundcube/config:/var/roundcube/config:ro
  networks:
    - mail
    - default
````

If you plan to use sieve filter, you'll need to add a configuration file in the config directory with

```
<?php
$config['managesieve_port'] = 4190;
$config['managesieve_host'] = 'tls://mail.example.com';
```
